#!/bin/bash

source ./internal_functions.sh
source ./docker_functions.sh
source ./group_functions.sh
source ./project_functions.sh
source ./test_functions.sh

while getopts ":g:o:p:i:n:df" opt; do
  case $opt in
    d) enable_debug
    ;;
    f) enable_dryrun
    ;;
    g) token="$OPTARG"
    ;;
    o) options="$OPTARG"
    ;;
    p) targetparent="$OPTARG"
    ;;
    i) nbImgs="$OPTARG"
    ;;
    n) nbProjects="$OPTARG"
    ;;
    \?) echo "⛔️ Invalid option -$OPTARG" >&2
    exit 1
    ;;
  esac

  case $OPTARG in
    -*) echo "⛔️ Option $opt needs a valid argument"
    exit 1
    ;;
  esac
done

init $2
options=$(echo $options | tr -d '\')
targetparent=$(echo $targetparent | tr -d '\')

parentoption="-s"
if [[ $targetparent != "" ]];then
  parentoption="-n"
fi

printf "${light_magenta}Test ${magenta}./migrate.sh -g $token -o $sg $parentoption ${targetparent}${tg} $options $debug_option $fake_enabled ${gray} \n"
printf "${light_magenta}~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n"
./migrate.sh -g $token -o $sg $parentoption ${targetparent}${tg} $options $debug_option $fake_enabled

if [ $nbProjects == 4 ];then
  checkTest $gs_id $nbProjects $nbImgs
else
  checkTest $gc_id $nbProjects $nbImgs
fi

result=$?

cleanData

exit $result
