################################
# Internal Functions for tests #
################################
source ./colors.sh

enable_debug() {
    debug_option="-d"
}

enable_dryrun() {
    fake_enabled="-f"
}

init() {
    printf "${yellow}Initialize test data${gray} \n"

    echo $GL_TOKEN

    if [ -z $GL_TOKEN ]; then
        GL_TOKEN=$1
    fi
    gitlab_uri=gitlab.com
    gitlab_registry=registry.gitlab.com
    random_id=$(( $RANDOM % 100 ))
    debug_enabled="n"
    dry_run="n"
    random=1
    test_parent_group="/fun_with/trash"

    if [ -z $DOCKER_TOKEN ]; then
    DOCKER_TOKEN=$GL_TOKEN
    fi

    checkDockerRegistryLogin $gitlab_registry
    checkDockerRunning

    if [ $random -eq 0 ]; then
    sg="fun_with_sourcegroup"
    else
    sg="fun_with_sourcegroup$random_id"
    fi
    creategroups ${test_parent_group:1}/${sg}
    curl --silent -o /dev/null --request POST --header "PRIVATE-TOKEN: $GL_TOKEN" "https://${gitlab_uri}/api/v4/groups/${g_id}/variables" --form "key=GROUP_VARIABLE" --form "value=the group"
    gs_id=$g_id

    d_img="oamdev/gcr.io-distroless-static:nonroot"
    docker pull $d_img

    d_img2="myaniu/gcr.io-distroless-static:latest"
    docker pull $d_img2

    # Project 1
    project="p1"
    createproject ${project} $gs_id
    p1_id=$p_id

    curl --silent -o /dev/null --request POST --header "PRIVATE-TOKEN: $GL_TOKEN" "https://${gitlab_uri}/api/v4/projects/${p1_id}/variables" --form "key=PROJECT_VARIABLE" --form "value=the project 1"

    docker tag  $d_img ${gitlab_registry}$test_parent_group/$sg/${project}/$d_img
    docker push ${gitlab_registry}$test_parent_group/$sg/${project}/$d_img

    docker tag  $d_img2 ${gitlab_registry}$test_parent_group/$sg/${project}/$d_img2
    docker push ${gitlab_registry}$test_parent_group/$sg/${project}/$d_img2

    # Project 2
    project="p2"
    createproject ${project} $gs_id
    p2_id=$p_id

    curl --silent -o /dev/null --request POST --header "PRIVATE-TOKEN: $GL_TOKEN" "https://${gitlab_uri}/api/v4/projects/${p2_id}/variables" --form "key=PROJECT_VARIABLE" --form "value=the project 2"

    archived_id=$p_id
    docker tag  $d_img ${gitlab_registry}$test_parent_group/$sg/${project}/$d_img
    docker push ${gitlab_registry}$test_parent_group/$sg/${project}/$d_img
    archiveProject $project $p2_id

    # Project 3
    project="p3"
    createproject ${project} $gs_id

    # Project 4
    project="p4"
    createproject ${project} $gs_id

    if [ $random -eq 0 ]; then
    tg="fun_with_targetgroup"
    else
    tg="fun_with_targetgroup$random_id"
    fi
    buildNewGroupPath $tg
    creategroups $new_group_name
    gc_id=$g_id
    for p in $(curl --silent --request GET --header "PRIVATE-TOKEN: $GL_TOKEN" "https://$gitlab_uri/api/v4/groups/$gc_id/projects" | jq ' .[] | .id'); do
    echo delete $p
    curl --silent --request DELETE --header "PRIVATE-TOKEN: $GL_TOKEN" "https://$gitlab_uri/api/v4/projects/$p"
    echo ""
    done

    printf $cyan"======================\n"
    printf $cyan" ✅ End of test setup  \n"
    printf $cyan"======================$gray\n"
}

checkTest() {

    printf $cyan"================\n"
    printf $cyan" 🚔 Test checks  \n"
    printf $cyan"================$gray\n"

    nb_img=$(curl --silent --header "PRIVATE-TOKEN: $GL_TOKEN"  "https://$gitlab_uri/api/v4/groups/$gc_id/registry/repositories" | jq '. | length')
    nb_projects=$(curl --silent --header "PRIVATE-TOKEN: $GL_TOKEN"  "https://$gitlab_uri/api/v4/groups/$1/projects" | jq '. | length')
    archived_check=$(curl --silent --header "PRIVATE-TOKEN: $GL_TOKEN"  "https://$gitlab_uri/api/v4/projects/$archived_id" | jq '. | .archived')

    var_group=$(curl --silent --header "PRIVATE-TOKEN: $GL_TOKEN"  "https://$gitlab_uri/api/v4/groups/$gs_id/variables" | jq '. | length')
    var_project1=$(curl --silent --header "PRIVATE-TOKEN: $GL_TOKEN"  "https://$gitlab_uri/api/v4/projects/$p1_id/variables" | jq '. | length')
    var_project2=$(curl --silent --header "PRIVATE-TOKEN: $GL_TOKEN"  "https://$gitlab_uri/api/v4/projects/$p2_id/variables" | jq '. | length')

    if [ $archived_check != "true" ]; then
        printf "${red}p2 was not re-archived. Test failed\n"
        return 1
    else
        printf "${green}p2 was re-archived ✅\n"
    fi

    if [ $var_group != 1 ]; then
        printf "${red}Missing variable at group level\n"
        return 1
    else
        printf "${green}Group variable is migrated ✅\n"
    fi

    if [ $var_project1 != 1 ]; then
        printf "${red}Missing variable at project level for p1\n"
        return 1
    else
        printf "${green}p1 variable is migrated ✅\n"
    fi

    if [ $var_project2 != 1 ]; then
        printf "${red}Missing variable at project level for p2\n"
        return 1
    else
        printf "${green}p2 variable is migrated ✅\n"
    fi

    if [[ $nb_img == $3 && $nb_projects == $2 ]]; then
        printf "${green}Test migration is ok\n"
        return 0
    fi

    if [ $nb_projects != $2 ]; then
        printf "${red}Test migration is ko : missing some projects ($nb_projects)\n"
    fi
    if [ $nb_img != $3 ]; then
        printf "${red}Test migration is ko : missing some images ($nb_img)\n"
    fi
    return 1

}

cleanData() {

    printf $cyan"==========================\n"
    printf $cyan" 🧹Test clean up  \n"
    printf $cyan"==========================$gray\n"

    printf "${gray}Delete groups\n"
    curl --silent --request DELETE --header "PRIVATE-TOKEN: $GL_TOKEN" "https://$gitlab_uri/api/v4/groups/$gc_id"
    printf "\n"
    curl --silent --request DELETE --header "PRIVATE-TOKEN: $GL_TOKEN" "https://$gitlab_uri/api/v4/groups/$gs_id"
    printf "\n"
}