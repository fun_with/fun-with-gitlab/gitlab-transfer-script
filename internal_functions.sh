######################
# Internal Functions #
######################
source ./colors.sh

## Print args
#############
debugargs() {
  printf "${yellow}GL_TOKEN = ${light_yellow}$GL_TOKEN${default}\n"
  printf "${yellow}old_group_name = ${light_yellow}$old_group_name${default}\n"
  printf "${yellow}new_group_name = ${light_yellow}$new_group_name${default}\n"
  printf "${yellow}gitlab_uri=${light_yellow}$gitlab_uri${default}\n"
  printf "${yellow}gitlab_registry=${light_yellow}$gitlab_registry${default}\n"
  debug_enabled="y"
}

## Handle fullgroup option
##########################
fullgroup_option() {
    if [[ "${1:0:1}" != '/' ]]; then
        printf ${red}'=======\n'
        printf ${light_blue}"-n option ${red}needs to start with / \n"
        usage
        exit 1
    fi
    new_group_name="$1"
}

## Enable dry run
#################
fakerun() {
  printf $yellow"🌵 Entering Dry mode$gray\n"
  dry_run="y"
}

## Enable debug mode
####################
debug() {
  if [ $debug_enabled = "y" ]; then
    printf "$light_yellow$1$gray\n"
  fi
}

## Print options
################
usage() {
  printf '=======\n'
  printf $light_gray"Usage : ./migrate.sh -g <GITLAB_TOKEN> -o <OLD_GROUP_NAME> -n <NEW_GROUP_NAME>\n"
  printf "=============================================================================\n"
  printf $green"Mandatory options\n"
  printf -- "-----------------\n"
  printf -- "-g : your gitlab API token\n"
  printf -- "-n : the full path of group that will contain the migrated projects\n"
  printf -- "-o : the group containing the projects you want to migrate\n"
  printf -- "-s : the simple path of group containing the projects you want to migrate, in same parent group then original one\n"
  printf -- "-----------------\n"
  printf $magenta"Other options\n"
  printf -- "-------------\n"
  printf -- "-d : parent group id for old group. This can be useful if several groups have the same name. (d is for daddy)"
  printf -- "-f : fake run\n"
  printf -- "-h : display usage\n"
  printf -- "-i : change gitlab instance. By default, it's gitlab.com 🦊\n"
  printf -- "-k : keep the group containing the project, it will be moved into group specified with -n\n"
  printf -- "-l : list projects to move if you want to keep some in origin group\n"
  printf -- "-p : password for 🐳 registry\n"
  printf -- "-r : change gitlab registry name if not registry.<gitlab_instance>. By default, it's registry.gitlab.com 🦊\n"
  printf -- "-t : filter tags to keep when moving 🐳 images & registries\n"  
  printf -- "-v : verbose mode to debug your migration\n"
}

## Debug a search query
#######################
debugsearch() {
    if [ ! -z $3 ]; then
        debug "curl --silent --request GET --header \"PRIVATE-TOKEN: $GL_TOKEN\" --header \"Content-Type: application/json\" \"https://$gitlab_uri/api/v4/groups?search=$1&per_page=200\" | jq --arg gn \"$1\" --arg attr \"$2\" --argjson fp $3 '.[] | select((.path==$gn) and (.parent_id==$fp)) | .[$attr]'"
    else 
        debug "curl --silent --request GET --header \"PRIVATE-TOKEN: $GL_TOKEN\" --header \"Content-Type: application/json\" \"https://$gitlab_uri/api/v4/groups?search=$1&per_page=200\" | jq --arg gn \"$1\" --arg attr \"$2\" '.[] | select(.path==$gn) | .[$attr]'"
    fi
}
