##############################
# Functions to manage groups #
##############################

## Build new path for group
###########################
buildNewGroupPath() {
  if [[ "${1:0:1}" != '/' ]]; then
    gparent_id=$(curl --silent --header "PRIVATE-TOKEN: $GL_TOKEN"  "https://$gitlab_uri/api/v4/groups/$g_id" | jq '. | .parent_id')
    gparent_path=$(curl --silent --header "PRIVATE-TOKEN: $GL_TOKEN"  "https://$gitlab_uri/api/v4/groups/$gparent_id" | jq '. | .full_path' | tr -d '"')
    debug "parent is $gparent_id in $gparent_path"
    new_group_name=$gparent_path/$1
  else
    new_group_name=${1:1}
  fi
}

## Create a group structure
###########################
creategroups(){
  group_name=$1
  debug "Creating structure $group_name"
  IFS='/' read -r -a group_array <<< "$group_name"
  for index in "${!group_array[@]}"; do
    if [ 0 -eq $index ]; then
        creategroup ${group_array[index]}
    else
        parent_i=$((index-1))
        creategroup ${group_array[index]} ${g_id}
    fi
  done
}

## Create a group
#################
creategroup() {
  groupname=$1
  parentid=$2

  printf $cyan"🔎 Check if group $light_blue$groupname$cyan already exist\n"
  g_id=$(searchgroup $groupname "id" $parentid)
  
  if [ $debug_enabled = "y" ]; then
    debugsearch $groupname "id" $parentid
  fi

  if [ ! -z $g_id ]; then
    printf "${green}🙈 Group ${light_blue}$1${green} already exist with id ${light_blue}$g_id${default}\n"
  else
    printf "${cyan}🎨 Group ${light_blue}$groupname${cyan} not found, creating it...\n"

    if [ -z $parentid ]; then
        if [ $dry_run = "y" ]; then
            printf -- "curl --silent --request POST --header \"PRIVATE-TOKEN: $GL_TOKEN\" --header \"Content-Type: application/json\" --data '{\"path\": \"'$groupname'\", \"name\": \"'$groupname'\"}' \"https://$gitlab_uri/api/v4/groups/\"\"\n"
            ok="999"   
        else
            cr=$(curl --silent --request POST --header "PRIVATE-TOKEN: $GL_TOKEN" --header "Content-Type: application/json" --data '{"path": "'$groupname'", "name": "'$groupname'"}' "https://$gitlab_uri/api/v4/groups/")
            if [ "$(echo $cr | grep -c "403")" -eq 1 ]; then
                printf "Creating group $groupname return => $cr\n"
            fi
            ok=$(echo $cr | jq " .id")
        fi
    else
        if [ $dry_run = "y" ]; then
            printf -- "curl --silent --request POST --header \"PRIVATE-TOKEN: $GL_TOKEN\" --header \"Content-Type: application/json\" --data '{\"parent_id\": \"$parentid\", \"path\": \"'$groupname'\", \"name\": \"'$groupname'\"}' \"https://$gitlab_uri/api/v4/groups/\"\"\n"
            ok="999"   
        else
            cr=$(curl --silent --request POST --header "PRIVATE-TOKEN: $GL_TOKEN" \
            --header "Content-Type: application/json" \
            --data '{"parent_id": "'$parentid'", "path": "'$groupname'", "name": "'$groupname'"}' \
            "https://$gitlab_uri/api/v4/groups/")
            ok=$(echo $cr | jq " .id")
        fi
    fi

    if [ "$ok" != "null" ] || [ $dry_run = "y" ]; then
      printf $green"✅ Group $light_blue$groupname$green create with id $light_blue$ok$gray\n"
      echo Group $1 create group_id : $ok >> migrate.log
      g_id=$ok
    else
      printf $red"⛔️ Impossible to create group $groupname :$gray $cr"
      exit 99
    fi
  fi
}

## Search a group
#################
searchgroup() {
    if [ ! -z $3 ] && [ "$3" != "" ]; then
        curl --silent --request GET --header "PRIVATE-TOKEN: $GL_TOKEN" --header "Content-Type: application/json" "https://$gitlab_uri/api/v4/groups?search=$1&per_page=200" | jq --arg gn "$1" --arg attr "$2" --argjson fp $3 '.[] | select((.path==$gn) and (.parent_id==$fp)) | .[$attr]'
    else 
        curl --silent --request GET --header "PRIVATE-TOKEN: $GL_TOKEN" --header "Content-Type: application/json" "https://$gitlab_uri/api/v4/groups?search=$1&per_page=200" | jq --arg gn "$1" --arg attr "$2" '.[] | select(.path==$gn) | .[$attr]'
    fi
}