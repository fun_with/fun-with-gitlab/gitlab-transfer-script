# Gitlab transfer script

The **migrate.sh** script helps you tranfer Gitlab 🦊 projects that contains images in Container registry from a group to another, as it's not possible through Gitlab UI.

## requirements

This script use the following commands : 
* jq
* curl
* docker (for docker image repository)

## 📚 Usage

```script
Usage : ./migrate.sh -g <GITLAB_TOKEN> -o <OLD_GROUP_NAME> -n <NEW_GROUP_NAME>
=============================================================================
Mandatory options
-----------------
-g : your gitlab API token
-n : the full path of group that will contain the migrated projects
-o : the group containing the projects you want to migrate
-s : the simple path of group containing the projects you want to migrate, in same parent group then original one
-----------------
Other options
-------------
-d : parent group id (if there are multiple with same name on the instance)
-f : fake run
-h : display usage
-i : change gitlab instance. By default, it's gitlab.com 🦊
-k : keep the group containing the project, it will be moved into group specified with -n
-l : list projects to move if you want to keep some in origin group
-p : password for 🐳 registry
-r : change gitlab registry name if not registry.<gitlab_instance>. By default, it's registry.gitlab.com 🦊
-t : filter tags to keep when moving 🐳 images & registries
-v : verbose mode to debug your migration
```

## 🧪 Test

If you want to fork/improve the script, you can use **test.sh** script to try a migration.
Beware, it will schedule the created groups for deletion, but may not delete them permanently, as it's not possible through API if the delayed deletion is activated on the instance.

The generated groups are hosted in [fun_with](https://gitlab.com/fun_with/trash) by default but are deleted at the end of the test.

To run it : `./test.sh <GITLAB_COM-TOKEN>`

## 🚸 Known limitations

For now:
- it only supports transfering in the same Gitlab instance.
- ~~it only tranfers projects containing images~~
