##########################################
# Internal Functions to interact with 🐳 #
##########################################

## Check Docker
################
checkDockerRunning(){
  #Check docker status
  out=$(docker ps)
  cr=$?
  if [ 1 -eq $cr ]; then
    printf "${red}⛔️ Docker not started${default}\n"
    printf "${red}$out${default}\n"
    printf "${red}🐳 You must first start docker daemon.${default}\n"
    printf "${light_blue}Use : sudo service docker start${default}\n"
    printf "${light_blue}I will try to do it for you.${default}\n"
    sudo service docker start
    fail=$?
    if [ 0 -eq $fail ]; then
      printf "${light_blue}🐳 Consider docker daemon start !.${default}\n"
    else
      printf "${yellow}😭 Sorry I don't succeed...${default}\n"
      exit 99
    fi
  fi
}

## Check Registry access
########################
checkDockerRegistryLogin(){
  gitlab_registry=$1
  #Check access to registry before anything
  docker_login=$(cat ~/.docker/config.json | grep "${gitlab_registry}")
  if [ -z "$docker_login" ]; then
    printf "${red}🚫 Not logged to gitlab registry${default}\n"
    printf "${red}You must first log in to target gitlab registry: $gitlab_registry.${default}\n"
    printf "${light_blue}🐳 Use : docker login $gitlab_registry -u USERNAME -p DOCKER_TOKEN${default}\n"
    printf "${light_blue}I will try to do it for you.${default}\n"
    cr=$(curl --silent --request GET --header "PRIVATE-TOKEN: $GL_TOKEN" --header "Content-Type: application/json" "https://$gitlab_uri/api/v4/user")
    ok=$(echo $cr | jq '.username' | tr -d '"')
    fail=1
    if [ ! -z "$ok" ]; then
      printf -- "docker login ${gitlab_registry} -u $ok -p $DOCKER_TOKEN"
      docker login ${gitlab_registry} -u $ok -p $DOCKER_TOKEN
      if [ 0 -eq $? ]; then
        if [ $(grep -c ${gitlab_registry} ~/.docker/config.json) -eq 1 ]; then
          fail=0
          printf "${light_blue}🐳 Consider Docker login as done !${default}\n"
        else
          fail=1
        fi
      fi
    fi
    if [ 1 -eq $fail ]; then
      printf "${red}😭 Sorry I don't succeed...${default}\n"
      printf "${cyan}Please log in to registry and re-run script${default}"
      exit 99
    fi
  fi
}

## List Images
################
getimages() {
  p_id=$1
  r_id=$2
  cr=$(curl --silent --header "PRIVATE-TOKEN: $GL_TOKEN" "https://$gitlab_uri/api/v4/projects/$p_id/registry/repositories/$r_id/tags")
  ok=$(echo $cr | jq '.[] | .location')
  if [ "$ok" != "null" ]; then

     if [[ $tags != "" ]];then
      imgs=$(echo $cr |  jq --arg prjs \"$tags\" '.[] | select(.name as $tmpvar | $prjs | index ($tmpvar)) | [.name, .path, .location] | @tsv' )
    else 
      imgs=$(echo $cr | jq '.[] | [.name, .path, .location] | @tsv')
    fi

    if [[ $imgs != "" ]]; then
      printf $green"🎞 Project $light_blue$1$green images in registry $light_blue$r_id$green are :$gray\n"
      printf "$white$imgs$gray\n"
      echo list images : $ok  >> migrate.log
    else
      printf $yellow"🎞 Project $light_blue$1$yellow has no images in registry after filtering$gray\n"
    fi
  else
    printf $red"💥 Error occured during image search on project $light_blue$p_id$red - repository $light_blue$r_id$gray\n"
    echo $cr | tee >> migrate.log
  fi
}

## Backup Images
################
backupImages(){

  p_id=$1
  r_id=$(curl --silent --header "PRIVATE-TOKEN: $GL_TOKEN"  "https://$gitlab_uri/api/v4/projects/$p_id/registry/repositories" | jq '.[] | .id' | tr -d "\"")
  all_imgs=()

  if [ -n "$r_id" ]; then

    for reg in $r_id ; do

        debug "Found registry with ID $reg"
        debug "Working on repository $reg from project $p_id\n" >> migrate.log

        getimages $p_id $reg

        if [[ $imgs != "" ]]; then
          printf $cyan"📩 Pulling existing images...\n"$gray
          for img in ${imgs} ; do
              img=$(echo $img | tr -d "\"" | sed -E 's:\\t: :g' | cut -d' ' -f3)
              all_imgs[${#all_imgs[@]}]=${img}
              
              echo ${img} >> images_${p_id}.tmp

              if [ $dry_run = "y" ]; then
                  printf -- "docker pull $img >/dev/null 2>&1\n"
              else
                  docker pull $img >/dev/null 2>&1
              fi
          done
        fi

        printf $cyan"🚮 Removing registry...\n"$gray
        if [ $dry_run = "y" ]; then
            printf -- "curl --silent --request DELETE --header \"PRIVATE-TOKEN: ***\" \"https://$gitlab_uri/api/v4/projects/$p_id/registry/repositories/$reg\"\n"
        else
            cr=$(curl --silent --request DELETE --header "PRIVATE-TOKEN: $GL_TOKEN" "https://$gitlab_uri/api/v4/projects/$p_id/registry/repositories/$reg")
        fi
        debug "Remove registry $reg on project $p_id ($p_name) : $cr"
        echo "🗑 Delete repository $reg for $p_name">> migrate.log
        echo $cr >> migrate.log

        if [ $dry_run != "y" ]; then
            # Tempo
            sleep 10
        fi
    done
  else
    printf $yellow"🤷 No registry found... continue...${gray}\n"
    r_enable="false"
  fi
}
