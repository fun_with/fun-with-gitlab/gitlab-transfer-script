################################
# Functions to manage projects #
################################

## Create a project
###################
createproject() {
  name=$1
  g_id=$2
  printf "${cyan}🦄 Create project $1 in $2${default}\n"
  curl --silent --request POST --header "PRIVATE-TOKEN: $GL_TOKEN" \
       --header "Content-Type: application/json" --data '{
          "name": "'$name'", "description": "'$name'", "path": "'$name'",
          "namespace_id": "'$g_id'", "initialize_with_readme": "true"}' \
       --url "https://$gitlab_uri/api/v4/projects/" > json
  if [ "$(grep -c "403" json)" -eq 1 ]; then
    printf "Creating project $1 return => $cr\n"
  fi
  p_id=$(jq " .id" json)
  if [ -z $p_id ]; then
     if [ "$(grep -c "has already been taken" json)" -gt 0 ]; then
         printf "${grey}Project $1 already exist${default}\n"
         p_id="null"
     else
       printf "${red}$(cat json)${default}"
       exit 99
     fi
  fi

  if [ "$p_id" != "null" ]; then
     printf "${green}✅ Project $light_blue$1${green} create with id ${light_blue}$p_id${green} in group ${light_blue}$g_id${gray}\n"
  else
     printf "${grey}📲 Get existing project id for $1${default}\n"
     curl --silent --header "PRIVATE-TOKEN: $GL_TOKEN" "https://$gitlab_uri/api/v4/groups/${g_id}/projects?search=$name" > json
     p_id=$(jq '.[] | select( .namespace.id | contains('$g_id')) | .id' json)
     if [ -z $p_id ]; then
       printf "${red}⛔️ Project ${light_blue}$1${red} id not found\n"
       debug "${red}$(cat json)${default}"
       exit 99
    else
      printf "${green}✅ Project ${light_blue}$1${green} found with id ${light_blue}$p_id${green} in group ${light_blue}$g_id${default}\n"
    fi
  fi
}

## Archive a project
###################
archiveProject() {
    printf $yellow"$1 was archived, re-archive it\n"$gray
    if [ $dry_run = "y" ]; then
      printf -- "curl --silent --request POST --header \"PRIVATE-TOKEN: $GL_TOKEN\" \"https://$gitlab_uri/api/v4/projects/$2/archive\"\n"
    else
      cr=$(curl --silent -o /dev/null -sw '%{http_code}' --request POST --header "PRIVATE-TOKEN: $GL_TOKEN" "https://$gitlab_uri/api/v4/projects/$2/archive")
      debug "cr is $cr"
      if [ $cr != "201" ]; then
        printf "${red}Unable to archive project, need to do it by hand \n"${gray}
      fi
    fi
}

## Unarchive a project
###################
unarchiveProject() {
    printf $yellow"$1 is archived, unarchive it temporarly\n"$gray
    if [ $dry_run = "y" ]; then
      printf -- "curl --silent --request POST --header \"PRIVATE-TOKEN: $GL_TOKEN\" \"https://$gitlab_uri/api/v4/projects/$2/unarchive\"\n"
    else
      cr=$(curl --silent -o /dev/null -sw '%{http_code}' --request POST --header "PRIVATE-TOKEN: $GL_TOKEN" "https://$gitlab_uri/api/v4/projects/$2/unarchive")
      debug "cr is $cr"
      if [ $cr != "201" ]; then
        printf "${red}Unable to unarchive project\n"${gray}
      fi      
    fi
}