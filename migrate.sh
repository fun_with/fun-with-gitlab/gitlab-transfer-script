#!/bin/bash
gitlab_name=gitlab.com
debug_enabled="n"
dry_run="n"
keep_parent="y"
projects_list=""
tags_list=""
parent_group_id=""

source ./internal_functions.sh
source ./docker_functions.sh
source ./group_functions.sh
source ./project_functions.sh

################
# Main process #
################
while getopts ":d:g:n:o:s:i:r:p:l:t:kvhf" opt; do
  case $opt in
    d) parent_group_id="$OPTARG"
    ;;
    f) fakerun
    ;;
    g) GL_TOKEN="$OPTARG"
    ;;
    h) usage && exit 0
    ;;
    i) gitlab_name="$OPTARG"
    ;;
    k) keep_parent="n"
    ;;
    l) projects_list="$OPTARG"
    ;;
    n) fullgroup_option $OPTARG
    ;;
    o) old_group_name="$OPTARG"
    ;;
    p) DOCKER_TOKEN="$OPTARG"
    ;;
    r) gitlab_registry="$OPTARG"
    ;;
    s) new_group_name="$OPTARG"
    ;;
    t) tags_list="$OPTARG"
    ;;    
    v) debug_enabled="y"
    ;;
    \?) echo "⛔️ Invalid option -$OPTARG" >&2
    exit 1
    ;;
  esac

  case $OPTARG in
    -*) echo "⛔️ Option $opt needs a valid argument"
    exit 1
    ;;
  esac
done

gitlab_uri=$gitlab_name
if [ -z $gitlab_registry ]; then
  gitlab_registry=registry.$gitlab_name
fi

if [ -z $DOCKER_TOKEN ]; then
  DOCKER_TOKEN=$GL_TOKEN
fi

if [ $debug_enabled = "y" ]; then
  debugargs
fi

# Check mandatory parameters
if [[ -z $GL_TOKEN || -z $old_group_name || -z $new_group_name ]]; then
  usage
  exit 1
fi

printf $cyan"🛫 Starting migration of $light_blue$old_group_name$cyan to $light_blue$new_group_name$cyan"

if [ $debug_enabled = "y" ]; then
  printf "$light_yellow (in debug)$gray \n"
else
  printf "\n"
fi

g_id=$(searchgroup $old_group_name "id" $parent_group_id)
g_fullpath=$(searchgroup $old_group_name "full_path" $parent_group_id)
g_path=$(searchgroup $old_group_name "path" $parent_group_id)

if [[ -z $g_id ]]; then
  printf "😢 ${red}Group $old_group_name not found${default}\n"
  exit 321
fi

debug "Found group with ID $g_id"

if [[ $projects_list != "" ]]; then
  debug "Project list is $projects_list"
  projects=()
  while IFS=',' read -ra PRJ; do
    for i in "${PRJ[@]}"; do
      projects+=$i
    done
  done <<< $projects_list
  debug "Filtered projects to migrate are $projects"
fi

if [[ $tags_list != "" ]]; then
  debug "Tags list to keep is $tags_list"
  tags=()
  while IFS=',' read -ra TAGS; do
    for i in "${TAGS[@]}"; do
      tags+=$i
    done
  done <<< $tags_list
  debug "Filtered tags to keep are $tags"
fi

pl=$(curl --silent --header "PRIVATE-TOKEN: $GL_TOKEN"  "https://$gitlab_uri/api/v4/groups/$g_id/projects")

if [[ $projects != "" ]]; then
  debug "Filtering projects"
  pl_ids=$(echo -E $pl |  jq --arg prjs \"$projects\" '.[] | select(.path as $tmpvar | $prjs | index ($tmpvar)) | [.id, .path, .container_registry_enabled, .archived] | @tsv' )
else
  debug "#nofilter"
  pl_ids=$(echo -E $pl |  jq '.[] | [.id, .path, .container_registry_enabled, .archived] | @tsv')
fi
pl_len=$(echo -E $pl |  jq '. | length' )

if [ "0" == "${pl_len}" ]; then
  printf "😢 ${magenta} No project found in group${default}\n"
  exit 1
fi

checkDockerRegistryLogin $gitlab_registry
checkDockerRunning

buildNewGroupPath $new_group_name
creategroups $new_group_name
gc_id=$g_id

for pl_id in $pl_ids; do
  
  p_id=$(echo $pl_id | tr -d "\"" | sed -E 's:\\t: :g' | cut -d' ' -f1)
  p_name=$(echo $pl_id | tr -d "\"" | sed -E 's:\\t: :g' | cut -d' ' -f2)
  r_enable=$(echo $pl_id | tr -d "\"" | sed -E 's:\\t: :g' | cut -d' ' -f3)
  p_archived=$(echo $pl_id | tr -d "\"" | sed -E 's:\\t: :g' | cut -d' ' -f4)

  if [[ $projects != "" ]]; then
    if ! [[ ${projects[*]} =~ "$p_name" ]]; then
      if [[ $r_enable = "false" && $keep_parent = "y" ]]; then
        printf -- "${light_red}${p_name} has no registry and will be migrated even if not in filtered list${default}"
      else 
        debug "Not migrating $p_name, not in filter list"
      fi
      continue
    fi
  fi

  printf $cyan"==========================\n"
  printf $cyan" Backup ${light_blue}$p_name${cyan} project  \n"
  printf $cyan"==========================$gray\n"

  if [ $p_archived = "true" ]; then
    unarchiveProject $p_name $p_id
  fi

  if [ $r_enable = "true" ]; then
    backupImages ${p_id}
  fi
done

if [ $keep_parent = "y" ]; then
  g_id=$(searchgroup $old_group_name "id" $parent_group_id)
  printf $cyan"⏩ Transfering $old_group_name to $new_group_name...\n"$gray
  printf -- "curl --silent --request POST --header \"PRIVATE-TOKEN: $GL_TOKEN\" \"https://$gitlab_uri/api/v4/groups/$g_id/transfer?group_id=$gc_id\"\n"
  if [ $dry_run = "y" ]; then
    printf -- "curl --silent --request POST --header \"PRIVATE-TOKEN: $GL_TOKEN\" \"https://$gitlab_uri/api/v4/groups/$g_id/transfer?group_id=$gc_id\"\n"
  else  
    cr=$(curl --silent -o /dev/null -sw '%{http_code}' --request POST --header "PRIVATE-TOKEN: $GL_TOKEN" "https://$gitlab_uri/api/v4/groups/$g_id/transfer?group_id=$gc_id")
    printf "Move result is $cr \n"
    if [[ $cr != "201" ]]; then
      printf -- "${red}Cannot move group, probably not empty...${gray} (error : $cr)"
      exit 99
    fi
  fi

  if [ $dry_run != "y" ]; then
    # Tempo
    sleep 10
  fi
fi

for pl_id in $pl_ids; do

  p_id=$(echo $pl_id | tr -d "\"" | sed -E 's:\\t: :g' | cut -d' ' -f1)
  p_name=$(echo $pl_id | tr -d "\"" | sed -E 's:\\t: :g' | cut -d' ' -f2)
  r_enable=$(echo $pl_id | tr -d "\"" | sed -E 's:\\t: :g' | cut -d' ' -f3)
  p_archived=$(echo $pl_id | tr -d "\"" | sed -E 's:\\t: :g' | cut -d' ' -f4)

  if [[ $projects != "" ]]; then
    if ! [[ ${projects[*]} =~ "$p_name" ]]; then
      debug "Not migrating $p_name, not in filter list"
      continue
    fi
  fi

  printf $cyan"==========================\n"
  printf $cyan" Restore ${light_blue}$p_name${cyan} project  \n"
  printf $cyan"==========================$gray\n"  

  if [ $keep_parent = "n" ]; then
    printf $cyan"⏩ Transfering ${light_blue}$p_name${cyan} to ${light_blue}$gc_id${cyan}...\n"$gray
    if [ $dry_run = "y" ]; then
      printf -- "curl --silent --request PUT --header \"PRIVATE-TOKEN: $GL_TOKEN\" \"https://$gitlab_uri/api/v4/projects/$p_id/transfer?namespace=$gc_id\"\n"
    else  
      cr=$(curl --silent -o /dev/null -sw '%{http_code}' --request PUT --header "PRIVATE-TOKEN: $GL_TOKEN" "https://$gitlab_uri/api/v4/projects/$p_id/transfer?namespace=$gc_id")
      printf "Move result is $cr \n"
    fi

    if [ $dry_run != "y" ]; then
      # Tempo
      sleep 10
    fi
  fi

  if [ -f "images_${p_id}.tmp" ]; then
    printf $cyan"✍️ Tagging & pushing existing images to new registry...\n"$gray

    while IFS= read -r img
    do
      # Push image to the new repository
      img=$(echo $img | tr -d '"')
      debug "image is ${img}"
      trim_gfpath=$(echo $g_fullpath | tr -d '"')
      trim_gpath=$(echo $g_path | tr -d '"')

      if [ $keep_parent = "y" ]; then
        new_img=$(echo $img | sed -E "s:${trim_gfpath}:${new_group_name}/${trim_gpath}:")
      else
        new_img=$(echo $img | sed -E "s:${trim_gfpath}:${new_group_name}:")
      fi
      debug "new_image is ${new_img} based on ${trim_gfpath} and ${new_group_name}"

      printf "✍️ ${cyan}Tag & push ${white}$new_img${default}\n"

      if [ $dry_run = "y" ]; then
        printf -- "docker tag $img $new_img \n"
        printf -- "docker push -q $new_img \n"
      else
        docker tag $img $new_img
        docker push -q $new_img
      fi
    done < images_${p_id}.tmp

    rm images_${p_id}.tmp
  fi

  if [ $p_archived = "true" ]; then
    archiveProject $p_name $p_id
  fi

  printf $cyan"=============================$gray \n"
  printf $cyan" 🛬 Migration of $p_name is$green ok 🎉\n"
  printf $cyan"=============================$gray \n"
done

if [ $dry_run = "y" ]; then
  printf -- "$green========================\n"
  printf    "= Dry run succeeded 🎉 =\n"
  printf -- "========================\n${default}"
fi
